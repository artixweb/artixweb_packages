# This file is part of Artix Web Packages. See LICENSE file for details
# Copyright 2022 - Artix Linux
FROM artixlinux/base

LABEL Maintainer="damnwidget@artixlinux.org"
LABEL Name="Artix Web Packages CI Container"
LABEL Version="0.1.0"

# update pacman
# install clang and pkg-config
# install rustup and put $HOME/.cargo/bin in the $PATH
# install stable and nightly toolchains
# install rustfmt-preview and clippy-preview in the nightly toolchain
RUN set -x \
    && pacman -Syu --noconfirm \
    && pacman -S --noconfirm clang pkgconf \
    && pacman -Scc --noconfirm \
    && curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y \
    && export PATH="$HOME/.cargo/bin:$PATH" \
    && rustup toolchain add stable nightly \
    && rustup component add --toolchain nightly rustfmt-preview clippy-preview

# Update PATH in the container
ENV PATH="/root/.cargo/bin:${PATH}"

