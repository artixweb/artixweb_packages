/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

/// The `Error` enum defines all the errors `artix_pkglib` can
/// return from its different actions
#[derive(Debug, thiserror::Error)]
pub enum Error<'a> {
    /// Raised by operations when information about a given package name can not be found
    #[error("could not find package {0} in any of the system present databases")]
    PackageNotFound(&'a str),
}
