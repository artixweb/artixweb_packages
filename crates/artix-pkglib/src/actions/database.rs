/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

//! This module implements common database extraction functions

use alpm::{Alpm, Db, Package};

/// Data structure used to returns information about an specific packages search
pub struct PackagesResultData<'a> {
    packages: Vec<&'a Package>,
    total: usize,
}

impl<'a> PackagesResultData<'a> {
    /// Retrieve the packages of this result if any
    #[must_use]
    pub fn packages(&self) -> &Vec<&Package> {
        &self.packages
    }

    /// Retrieve the total number of matched packages in the criteria used for this result
    #[must_use]
    pub fn total(&self) -> usize {
        self.total
    }
}

/// Returns back all the packages contained in the given database
/// If the database is invalid it returns None
#[must_use]
pub fn get_packages<'a>(db: &'a Db, limit: usize, offset: usize) -> Vec<&'a Package> {
    let mut ret = Vec::new();
    if db.is_valid().is_ok() {
        let pkgs = db.pkgs();
        ret.extend(pkgs);

        // if offset is set, truncate the vector at the given offset
        if offset != 0 && offset < ret.len() {
            ret = ret.split_off(offset);
        }

        // if limit is set, truncate the vector at it
        if limit != 0 && limit < ret.len() {
            ret.truncate(limit);
        }
    }

    ret
}

/// Returns all the packages contained with-in all registered databases
#[must_use]
#[allow(clippy::needless_pass_by_value)]
pub fn get_all_packages<'a>(
    alpm: &'a Alpm,
    limit: usize,
    offset: usize,
    repos: Option<Vec<&str>>,
    keyword: Option<&str>,
) -> PackagesResultData<'a> {
    let mut ret = Vec::new();
    for db in alpm.syncdbs() {
        if let Some(r) = &repos {
            if !r.contains(&db.name()) {
                continue;
            }
        }

        if let Some(kw) = keyword {
            if let Ok(pkgs) = db.search([kw].iter()) {
                ret.extend(pkgs);
            }
        } else {
            let pkgs = db.pkgs();
            ret.extend(pkgs);
        }
    }

    if ret.is_empty() {
        return PackagesResultData {
            packages: Vec::new(),
            total: 0,
        };
    }

    let total = ret.len();
    let max_offset: usize = total / limit.max(1);

    // sort the packages by name before applying split or truncate
    ret.sort_by(|a, b| a.name().cmp(b.name()));
    // if offset is set, split the vector at the given offset
    if offset != 0 {
        ret = ret
            .split_at_mut(offset.clamp(0, max_offset) * limit)
            .1
            .to_vec();
    }

    // if limit is set, truncate the vector at the given offset
    if limit != 0 {
        ret.truncate(limit);
    }

    PackagesResultData {
        packages: ret,
        total,
    }
}

#[cfg(test)]
mod test {
    use alpm::SigLevel;

    use super::*;

    #[test]
    fn test_get_packages() {
        let db_location = format!("{}/tests/db", env!("CARGO_MANIFEST_DIR"));
        let alpm = Alpm::new("/", &db_location).unwrap();
        let db = alpm.register_syncdb("world", SigLevel::NONE).unwrap();

        let pkgs = get_packages(*db, 0, 0);
        assert!(!pkgs.is_empty());

        let pkgs = get_packages(*db, 2, 0);
        assert_eq!(pkgs.len(), 2);

        let pkgs = get_packages(*db, 0, 0);
        let pkgs2 = get_packages(*db, 0, 10);
        assert_eq!(pkgs2[0].name(), pkgs[10].name());
    }

    #[test]
    fn test_get_all_packages() {
        let db_location = format!("{}/tests/db", env!("CARGO_MANIFEST_DIR"));
        let alpm = Alpm::new("/", &db_location).unwrap();
        alpm.register_syncdb("world", SigLevel::USE_DEFAULT)
            .unwrap();
        alpm.register_syncdb("galaxy", SigLevel::USE_DEFAULT)
            .unwrap();
        alpm.register_syncdb("system", SigLevel::USE_DEFAULT)
            .unwrap();

        let pkgs = get_all_packages(&alpm, 0, 0, None, None);
        assert!(!pkgs.packages.is_empty());

        let pkgs = get_all_packages(&alpm, 2, 0, None, None);
        assert_eq!(pkgs.packages.len(), 2);

        let pkgs = get_all_packages(&alpm, 11, 0, None, None);
        let pkgs2 = get_all_packages(&alpm, 10, 1, None, None);
        assert_eq!(pkgs2.packages[0].name(), pkgs.packages[10].name());
    }
}
