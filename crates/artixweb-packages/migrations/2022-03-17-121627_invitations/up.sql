-- Table 'invitations' is used to store new user creation invitations
CREATE TABLE invitations (
    id UUID NOT NULL UNIQUE PRIMARY KEY,
    email VARCHAR(100) NOT NULL,
    expires_at TIMESTAMP NOT NULL
)