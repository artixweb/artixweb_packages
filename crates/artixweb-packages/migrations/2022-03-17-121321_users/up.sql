-- Table 'users' is used to store information about artixweb users
CREATE TABLE users (
    email VARCHAR(100) NOT NULL UNIQUE PRIMARY KEY,
    hash VARCHAR(122) NOT NULL,  -- argon hash
    created_at TIMESTAMP NOT NULL,
    banned BOOLEAN NOT NULL DEFAULT 'f'
    admin BOOLEAN NOT NULL DEFAULT 'f'
)