/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use std::{env, fs::File, io::Read, path::Path};

use argon2::{password_hash::SaltString, Argon2, PasswordHash, PasswordHasher, PasswordVerifier};
use artix_pkglib::prelude::{Alpm, SigLevel};
use rand_core::OsRng;

use super::lib::errors::ServiceError;

lazy_static::lazy_static! {
    /// Global constant application settings
    pub static ref SETTINGS: AppSettings = AppSettings {
        app_name: env::var("APP_NAME").unwrap_or_else(|_| String::from("ArtixWeb Packages")),
        gitea_url: env::var("GITEA_URL").unwrap_or_else(|_| String::from(artix_gitea::prelude::GITEA_URL)),
        gitea_api_url: env::var("GITEA_API_URL").unwrap_or_else(|_| String::from(artix_gitea::prelude::GITEA_API_URL)),
        gitea_token: env::var("GITEA_TOKEN").unwrap_or_else(|_| String::new()),
        databases_path: env::var("DATABASES_PATH").unwrap_or_else(|_| String::from("/var/lib/pacman")),
        api_token: env::var("API_TOKEN").unwrap_or_else(|_| generate_random_token()),
    };
}

lazy_static::lazy_static! {
    pub static ref SECRET_KEY: String = std::env::var("SECRET_KEY").unwrap_or_else(|_| generate_random_token());
}

/// Data structure containing the site configuration
pub struct AppSettings {
    pub app_name: String,
    pub gitea_url: String,
    pub gitea_api_url: String,
    pub gitea_token: String,
    pub databases_path: String,
    pub api_token: String,
}

impl Default for AppSettings {
    fn default() -> Self {
        Self {
            app_name: String::from("ArtixWeb Packages"),
            gitea_url: String::from(artix_gitea::prelude::GITEA_URL),
            gitea_api_url: String::from(artix_gitea::prelude::GITEA_API_URL),
            gitea_token: String::new(),
            databases_path: String::from("/var/lib/pacman"),
            api_token: generate_random_token(),
        }
    }
}

/// Data structure containing SMTP configuration
#[derive(Clone)]
pub struct SmtpOptions {
    pub user: String,
    pub password: String,
    pub relay: String,
}

fn generate_random_token() -> String {
    use std::fmt::Write;
    let path = Path::new("/dev/urandom");
    let mut file = File::open(&path).unwrap();
    let mut buffer = [0; 32];
    file.read_exact(&mut buffer[..]).unwrap();
    let mut token = String::new();
    for byte in buffer {
        write!(&mut token, "{:x}", byte).expect("Unable to write");
    }
    token
}

const DATABASESS: [&str; 8] = [
    "system",
    "world",
    "galaxy",
    "lib32",
    "system-gremlins",
    "world-gremlins",
    "galaxy-gremlins",
    "lib32-gremlins"
];
const MIN_PASSWORD_LENGTH: usize = 8;

/// Syncs the configured databases
pub fn sync_databases(alpm: &Alpm) {
    for db in DATABASESS {
        alpm.register_syncdb(db, SigLevel::USE_DEFAULT).unwrap();
    }
}

/// hash the given password with argon2
pub fn hash_password(password: &str) -> Result<String, ServiceError> {
    let pwd = password.as_bytes();
    let salt = SaltString::generate(&mut OsRng);

    let argon2 = Argon2::default();
    if let Ok(password_hash) = argon2.hash_password(pwd, &salt) {
        Ok(password_hash.to_string())
    } else {
        Err(ServiceError::InternalServerError)
    }
}

// verify a password
pub fn verify(hash: &str, password: &str) -> Result<bool, ServiceError> {
    let argon2 = Argon2::default();

    if let Ok(parsed_hash) = PasswordHash::new(hash) {
        if argon2
            .verify_password(password.as_bytes(), &parsed_hash)
            .is_ok()
        {
            return Ok(true);
        }
    }

    Err(ServiceError::Unauthorized)
}

// check password strength
pub fn check_password_strength(password: &str) -> bool {
    if password.len() < MIN_PASSWORD_LENGTH {
        return false;
    }

    let mut punctuation_found = false;
    let mut caps_found = false;
    for c in password.chars() {
        match c {
            '.' | ',' | '$' | '%' | ';' | ':' | '@' | '_' | '-' | '!' | '?' => {
                punctuation_found = true;
                if caps_found {
                    break;
                }
            }
            _ => {
                if c.is_uppercase() {
                    caps_found = true;
                    if punctuation_found {
                        break;
                    }
                }
            }
        }
    }

    if !punctuation_found || !caps_found {
        return false;
    }

    true
}
