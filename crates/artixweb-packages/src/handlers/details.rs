/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

#![allow(clippy::unused_async, clippy::module_name_repetitions)]
use actix_session::Session;
use actix_web::{get, web, HttpResponse, Result};
use askama::Template;

use crate::models::Pool;

use super::packages::{get_packages_details_inner, ResponseDetail};

#[derive(Template)]
#[template(path = "package_details.html")]
struct PackageDetailsTemplate {
    pkg: ResponseDetail,
    user_email: String,
    generation_time: u128,
}

#[derive(Template)]
#[template(path = "error.html")]
struct PackageNotFoundTemplate {
    error_message: String,
    user_email: String,
    generation_time: u128,
}

/// Construct the package detail interface for the web site
#[get("/details/{package_name}")]
pub async fn package_details(
    data: web::Path<String>,
    session: Session,
    pool: web::Data<Pool>,
) -> Result<HttpResponse> {
    let start_time = std::time::Instant::now();
    let user_email = if let Some(email) = session.get::<String>("user_email").unwrap() {
        email
    } else {
        String::new()
    };

    let s = if let Ok(details) = get_packages_details_inner(&data, pool).await {
        PackageDetailsTemplate {
            user_email,
            pkg: details,
            generation_time: start_time.elapsed().as_millis(),
        }
        .render()
        .unwrap()
    } else {
        return Ok(HttpResponse::NotFound().content_type("text/html").body(
            PackageNotFoundTemplate {
                user_email,
                error_message: format!(
                    "The specified package {} could not be found in the Artix packages database.",
                    data
                ),
                generation_time: start_time.elapsed().as_millis(),
            }
            .render()
            .unwrap(),
        ));
    };

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[allow(clippy::unnecessary_wraps)]
mod filters {
    use chrono::NaiveDateTime;

    use crate::handlers::packages::Dependency;

    pub fn details_url(pkg_name: &str) -> ::askama::Result<String> {
        Ok(super::super::details_url(pkg_name))
    }

    #[allow(clippy::cast_precision_loss, clippy::trivially_copy_pass_by_ref)]
    pub fn human_readable(size: &i64) -> ::askama::Result<String> {
        let float_size = *size as f64 / 1024.0 / 1024.0;
        let human_size = format!("{:.2}", float_size);
        Ok(human_size)
    }

    #[allow(clippy::trivially_copy_pass_by_ref)]
    pub fn show_date(timestamp: &i64) -> ::askama::Result<NaiveDateTime> {
        Ok(NaiveDateTime::from_timestamp(*timestamp, 0))
    }

    pub(crate) fn provides_or_replaces(deps: &[Dependency]) -> ::askama::Result<String> {
        let mut elements = Vec::new();
        elements.extend(
            deps.iter()
                .map(|dep| format!("{}{}{}", dep.name, dep.depmod, dep.ver)),
        );
        Ok(elements.join(", "))
    }
}
