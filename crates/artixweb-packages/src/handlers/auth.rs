/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use std::future::{ready, Ready};

use actix_identity::Identity;
use actix_session::Session;
use actix_web::{dev::Payload, http, web, Error, FromRequest, HttpRequest, HttpResponse};
use askama::Template;
use diesel::prelude::*;
use diesel::PgConnection;
use serde::Deserialize;

use crate::config::verify;
use crate::lib::errors::ServiceError;
use crate::models::{Pool, SlimUser, User};

#[derive(Debug, Deserialize)]
pub struct Data {
    pub email: String,
    pub password: String,
}

pub type LoggedUser = SlimUser;

impl FromRequest for LoggedUser {
    type Error = Error;
    type Future = Ready<Result<LoggedUser, Error>>;

    fn from_request(req: &HttpRequest, pl: &mut Payload) -> Self::Future {
        if let Ok(identity) = Identity::from_request(req, pl).into_inner() {
            if let Some(user_data) = identity.identity() {
                if let Ok(user) = serde_json::from_str(&user_data) {
                    return ready(Ok(user));
                }
            }
        }

        ready(Err(ServiceError::Unauthorized.into()))
    }
}

#[derive(Template)]
#[template(path = "login_ui.html")]
struct LoginUITemplate {
    user_email: String,
    generation_time: u128,
}

#[allow(clippy::unused_async)]
pub async fn ui(session: Session) -> HttpResponse {
    let start_time = std::time::Instant::now();
    if session.get::<String>("user_email").unwrap().is_some() {
        return HttpResponse::SeeOther()
            .append_header((http::header::LOCATION, "/"))
            .finish();
    }

    let s = LoginUITemplate {
        user_email: String::new(),
        generation_time: start_time.elapsed().as_millis(),
    }
    .render()
    .unwrap();

    HttpResponse::Ok().content_type("text/html").body(s)
}

pub async fn login(
    auth_data: web::Form<Data>,
    id: Identity,
    session: Session,
    pool: web::Data<Pool>,
) -> Result<HttpResponse, actix_web::Error> {
    let user = web::block(move || query(&auth_data.into_inner(), &pool)).await??;
    let user_string = serde_json::to_string(&user).unwrap();
    id.remember(user_string);
    session.insert("user_email", user.email)?;
    Ok(HttpResponse::SeeOther()
        .append_header((http::header::LOCATION, "/"))
        .finish())
}

#[allow(clippy::unused_async)]
pub async fn logout(id: Identity, session: Session) -> HttpResponse {
    id.forget();
    session.remove("user_email");
    HttpResponse::SeeOther()
        .append_header((http::header::LOCATION, "/"))
        .finish()
}

fn query(auth_data: &Data, pool: &web::Data<Pool>) -> Result<SlimUser, ServiceError> {
    use crate::schema::users::dsl::{email, users};
    let conn: &PgConnection = &pool.get().unwrap();
    let mut items = users
        .filter(email.eq(&auth_data.email))
        .load::<User>(conn)?;

    if let Some(user) = items.pop() {
        if let Ok(matching) = verify(&user.hash, &auth_data.password) {
            if matching {
                return Ok(user.into());
            }
        }
    }
    Err(ServiceError::Unauthorized)
}
