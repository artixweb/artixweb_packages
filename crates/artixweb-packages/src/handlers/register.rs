/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use actix_session::Session;
use actix_web::{http, web, HttpResponse};
use askama::Template;
use diesel::prelude::*;
use diesel::PgConnection;
use serde::Deserialize;

use crate::config::{check_password_strength, hash_password};
use crate::lib::errors::ServiceError;
use crate::models::{Invitation, Pool, SlimUser, User};

#[derive(Debug, Deserialize)]
pub struct UserData {
    pub password: String,
    pub repeat_password: String,
}

#[derive(Template)]
#[template(path = "base_register.html")]
struct BaseRegisterTemplate {
    invitation_id: String,
    user_email: String,
    generation_time: u128,
}

#[derive(Template)]
#[template(path = "register.html")]
struct RegisterTemplate {
    invitation_id: String,
    user_email: String,
    generation_time: u128,
}

#[derive(Template)]
#[template(path = "invalid_invitation.html")]
struct InvalidInvitationTemplate {
    invitation_id: String,
    user_email: String,
    generation_time: u128,
}

#[derive(Template)]
#[template(path = "weak_password.html")]
struct WeakPasswordTemplate {
    invitation_id: String,
    user_email: String,
    generation_time: u128,
}

#[allow(clippy::unused_async)]
pub async fn invitation(
    invitation_id: web::Path<String>,
    pool: web::Data<Pool>,
    session: Session,
) -> Result<HttpResponse, actix_web::Error> {
    let start_time = std::time::Instant::now();
    if session.get::<String>("user_email").unwrap().is_some() {
        return Ok(HttpResponse::SeeOther()
            .append_header((http::header::LOCATION, "/"))
            .finish());
    }

    let valid_invitation = check_invitation(&invitation_id, &pool)?;

    let s = if valid_invitation {
        RegisterTemplate {
            user_email: String::new(),
            invitation_id: invitation_id.into_inner(),
            generation_time: start_time.elapsed().as_millis(),
        }
        .render()
        .unwrap()
    } else {
        InvalidInvitationTemplate {
            user_email: String::new(),
            invitation_id: invitation_id.into_inner(),
            generation_time: start_time.elapsed().as_millis(),
        }
        .render()
        .unwrap()
    };

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

fn check_invitation(
    invitation_id: &str,
    pool: &web::Data<Pool>,
) -> Result<bool, crate::lib::errors::ServiceError> {
    use crate::schema::invitations::dsl::{id, invitations};

    let invitation_id = uuid::Uuid::parse_str(invitation_id)?;
    let conn: &PgConnection = &pool.get().unwrap();

    let inv = invitations
        .filter(id.eq(invitation_id))
        .load::<Invitation>(conn)
        .map_err(|_db_error| {
            ServiceError::BadRequest("Invalid or expired Invitation".into(), None)
        })?;

    Ok(!inv.is_empty())
}

pub async fn new_user(
    invitation_id: web::Path<String>,
    user_data: web::Form<UserData>,
    pool: web::Data<Pool>,
) -> Result<HttpResponse, actix_web::Error> {
    let start_time = std::time::Instant::now();
    // check that the password is "good enough"
    let user_data = user_data.into_inner();
    let password = user_data.password;
    let repeat_password = user_data.repeat_password;

    if password != repeat_password {
        return Err(ServiceError::BadRequest(
            String::from("Introduced passwords does not match!"),
            Some(start_time),
        )
        .into());
    }

    if !check_password_strength(&password) {
        return Ok(HttpResponse::Ok().content_type("text/html").body(
            WeakPasswordTemplate {
                user_email: String::new(),
                invitation_id: invitation_id.into_inner(),
                generation_time: start_time.elapsed().as_millis(),
            }
            .render()
            .unwrap(),
        ));
    }

    // create user from the input data in the form
    web::block(move || query(&invitation_id.into_inner(), &password, &pool)).await??;
    Ok(HttpResponse::SeeOther()
        .append_header((http::header::LOCATION, "/login"))
        .finish())
}

fn query(
    invitation_id: &str,
    password: &str,
    pool: &web::Data<Pool>,
) -> Result<SlimUser, crate::lib::errors::ServiceError> {
    use crate::schema::invitations::dsl::{id, invitations};
    use crate::schema::users::dsl::users;

    let start_time = std::time::Instant::now();
    let invitation_id = uuid::Uuid::parse_str(invitation_id)?;
    let conn: &PgConnection = &pool.get().unwrap();
    invitations
        .filter(id.eq(invitation_id))
        .load::<Invitation>(conn)
        .map_err(|_db_error| {
            ServiceError::BadRequest(
                format!("Invalid Invitation ID: {}", invitation_id),
                Some(start_time),
            )
        })
        .and_then(|mut result| {
            if let Some(invitation) = result.pop() {
                // if invitation is not expired
                if invitation.expires_at > chrono::Local::now().naive_local() {
                    // try hashing the password, else return the error that will be converted to ServiceError
                    let password = hash_password(password)?;

                    let user = User::from_details(invitation.email, password);
                    let inserted_user: User =
                        diesel::insert_into(users).values(&user).get_result(conn)?;

                    return Ok(inserted_user.into());
                }
            }

            Err(ServiceError::BadRequest(
                format!("Invalid Invitation ID: {}", invitation_id),
                Some(start_time),
            ))
        })
}
