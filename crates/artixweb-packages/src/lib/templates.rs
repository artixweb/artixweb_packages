/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use askama::Template;

use crate::models::Invitation;

/// `BaseTemplate` is used as base canvas for the included simple UI
#[derive(Template)]
#[template(path = "base.html")]
pub struct BaseTemplate {
    user_email: String,
    generation_time: u128,
}

#[derive(Template)]
#[template(path = "invitation_email.html")]
pub struct EmailTemplate<'a> {
    pub(crate) invitation: &'a Invitation,
}

#[derive(Template)]
#[template(path = "package_flag_email.html")]
pub struct PackageFlagEmail {
    pub(crate) package_name: String,
    pub(crate) flag_by: String,
    pub(crate) message: String,
}
