/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use artix_pkglib::prelude::{get_all_packages, Alpm, PackagesResultData};

use crate::lib::errors::ArtixWebPackageError;

/// Retrieve all the packages in all the databases in the system
/// If `limit` is used then the results are limited to its value
/// also if `offset` is used, the results will start from its value
pub fn packages<'a>(
    alpm: &'a Alpm,
    limit: usize,
    offset: usize,
    repos: Option<Vec<&str>>,
    keyword: Option<&str>,
) -> Result<PackagesResultData<'a>, ArtixWebPackageError> {
    let pkgs = get_all_packages(alpm, limit, offset, repos, keyword);
    if pkgs.packages().is_empty() {
        Err(ArtixWebPackageError::NotFound)
    } else {
        Ok(pkgs)
    }
}
