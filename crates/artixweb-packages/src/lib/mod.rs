/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

//! `ArtixWeb` Packages Library.
//!
//! This internal library gives support to both API and basic HTML Web output

pub mod database;
pub mod errors;
pub mod templates;
