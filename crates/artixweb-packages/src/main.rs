/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

//! This crate provides an HTTP API to work with Artix Packages

#![deny(
    nonstandard_style,
    rust_2018_idioms,
    rust_2021_compatibility,
    missing_docs
)]
#![warn(clippy::all, clippy::pedantic)]

#[macro_use]
extern crate diesel;

mod config;
mod handlers;
mod lib;
mod models;
mod routes;
mod schema;

use actix_identity::{CookieIdentityPolicy, IdentityService};
use actix_session::{storage::CookieSessionStore, SessionMiddleware};
use actix_web::{
    cookie::Key,
    middleware::{Compress, Logger},
    web, App, HttpServer,
};
use clap::{arg, command};
use diesel::prelude::*;
use diesel::r2d2::ConnectionManager;
use log::{debug, warn};
use time::Duration;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // parse config
    let matches = command!()
        .arg(
            arg!(-b --bind <BIND_ADDRESS>)
                .required(false)
                .default_value("127.0.0.1")
                .env("BIND_ADDRESS"),
        )
        .arg(arg!(-p --port <PORT>).required(false).default_value("1936"))
        .arg(
            arg!(-u --databaseurl <DATABASE_URL>)
                .required(false)
                .default_value("localhost")
                .env("DATABASE_URL"),
        )
        .arg(
            arg!(-d --domain <DOMAIN>)
                .required(false)
                .default_value("localhost"),
        )
        .arg(
            arg!(-k --key <SESSION_KEY>)
                .required(true)
                .env("SESSION_KEY"),
        )
        .arg(
            arg!(--smtp_user <SMTP_USER>)
                .required(true)
                .env("SMTP_USER"),
        )
        .arg(arg!(--smtp_pwd <SMTP_PWD>).required(true).env("SMTP_PWD"))
        .arg(
            arg!(--smtp_relay <SMTP_RELAY>)
                .required(true)
                .env("SMTP_RELAY"),
        )
        .arg(arg!(-v - -verbose ... ))
        .get_matches();

    // count how many appearances of verbose we got and assign a value for env_logger
    let debug_level = match matches.occurrences_of("verbose") {
        0 => "warn",
        1 => "info",
        _ => "debug",
    };

    // configure logger
    env_logger::init_from_env(env_logger::Env::default().default_filter_or(debug_level));
    debug!("environment logger level set to 'debug'");

    let host_addr = matches.value_of("bind").unwrap_or_default();
    let port = matches.value_of("port").unwrap_or_default();
    let host_port = if let Ok(n) = port.parse::<u16>() {
        n
    } else {
        warn!("could not parse {} host_port, defaulting to 1936", port);
        1936
    };

    let database_url = matches.value_of("databaseurl").unwrap_or_default();
    let domain = matches.value_of("domain").unwrap_or_default();
    let master_key = matches.value_of("key").unwrap();

    let smtp_config = config::SmtpOptions {
        user: String::from(matches.value_of("smtp_user").unwrap_or_default()),
        password: String::from(matches.value_of("smtp_pwd").unwrap_or_default()),
        relay: String::from(matches.value_of("smtp_relay").unwrap_or_default()),
    };

    // start http server
    debug!("starting Artix web server on {}:{}", host_addr, host_port);
    start_web_server(
        host_addr,
        host_port,
        database_url,
        master_key.as_bytes(),
        domain.to_string(),
        smtp_config,
    )
    .await
}

// starts the web server with the given configuration
async fn start_web_server(
    host_addr: &str,
    host_port: u16,
    database_url: &str,
    master_key: &[u8],
    domain: String,
    smtp_config: config::SmtpOptions,
) -> std::io::Result<()> {
    // create PostgreSQL connection pool
    let manager = ConnectionManager::<PgConnection>::new(database_url);
    let pool: models::Pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create PostgreSQL pool.");

    let secret_key = Key::derive_from(master_key);
    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(pool.clone()))
            .app_data(web::Data::new(smtp_config.clone()))
            .configure(routes::config_app)
            .wrap(Logger::default())
            .wrap(Compress::default())
            .wrap(SessionMiddleware::new(
                CookieSessionStore::default(),
                secret_key.clone(),
            ))
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(config::SECRET_KEY.as_bytes())
                    .name("auth")
                    .path("/")
                    .domain(&domain)
                    .max_age(Duration::days(1))
                    .secure(false),
            ))
    })
    .bind((host_addr, host_port))?
    .run()
    .await
}
