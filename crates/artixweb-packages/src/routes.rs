/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use actix_files::Files;
use actix_web::{guard, web, HttpResponse};

use crate::handlers::{auth, details, index, invitation, packages, register};

/// Configure the application router
pub fn config_app(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("")
            .service(web::resource("/api/invitation").route(web::post().to(invitation::post)))
            .service(
                web::scope("/api/packages")
                    .service(
                        web::resource("/{repo}/{limit}/{offset}")
                            .route(web::get().to(packages::get_packages)),
                    )
                    .service(web::scope("/{package_name}").service(
                        web::resource("").route(web::get().to(packages::get_package_details)),
                    ))
                    .default_service(
                        web::route()
                            .guard(guard::Not(guard::Get()))
                            .to(HttpResponse::MethodNotAllowed),
                    ),
            )
            .service(web::resource("/").route(web::get().to(index::index)))
            .service(details::package_details)
            .service(
                web::resource("/register/{invitation_id}")
                    .route(web::get().to(register::invitation)),
            )
            .service(
                web::resource("/new_user/{invitation_id}")
                    .route(web::post().to(register::new_user)),
            )
            .service(
                web::resource("/login")
                    .route(web::post().to(auth::login))
                    .route(web::delete().to(auth::logout))
                    .route(web::get().to(auth::ui)),
            )
            .service(web::resource("/logout").route(web::post().to(auth::logout)))
            .service(
                web::resource("/flag_package/{package_name}/{package_version}")
                    .route(web::post().to(packages::flag_package))
                    .route(web::get().to(packages::flags_package_ui))
                    .route(web::delete().to(packages::flag_package_delete)),
            )
            .service(Files::new("/assets/images", "assets/images"))
            .service(Files::new("/assets/css", "assets/css/")),
    );
}
