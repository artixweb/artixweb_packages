table! {
    invitations (id) {
        id -> Uuid,
        email -> Varchar,
        expires_at -> Timestamp,
    }
}

table! {
    package_flags (user_email, package_name) {
        user_email -> Varchar,
        package_name -> Varchar,
        flag_on -> Timestamp,
    }
}

table! {
    packages (package_name) {
        package_name -> Varchar,
        gitea_url -> Varchar,
        last_update -> Timestamp,
    }
}

table! {
    users (email) {
        email -> Varchar,
        hash -> Varchar,
        created_at -> Timestamp,
        banned -> Bool,
    }
}

joinable!(package_flags -> packages (package_name));
joinable!(package_flags -> users (user_email));

allow_tables_to_appear_in_same_query!(invitations, package_flags, packages, users,);
