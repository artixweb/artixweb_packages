/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

mod config;
mod models;

use awc::Client;

// re-export both gitea url and gitea api url
pub use config::{GITEA_API_URL, GITEA_URL};

// re-export models::Repository
pub use models::Repository;

/// Returns a `Vec<String>` containing the 'packages<Letter>' organization
/// ids present in Artix Gitea API
///
/// If there is an error reaching gitea, returns `None`.
pub async fn get_organization_ids(gitea_api_url: Option<&str>) -> Option<Vec<String>> {
    let client = Client::default();

    let response = client
        .get(format!(
            "{}/orgs",
            gitea_api_url.unwrap_or(config::GITEA_API_URL)
        ))
        .insert_header((
            "User-Agent",
            format!("{}/{}", config::USER_AGENT, config::VERSION),
        ))
        .send()
        .await;

    if let Ok(mut res) = response {
        if res.status().is_success() {
            if let Ok(data) = res.json::<Vec<models::Org>>().await {
                let names: Vec<String> = data
                    .iter()
                    .filter(|org| {
                        org.username().starts_with("packages")
                            || org.username().contains("Universe")
                    })
                    .map(models::Org::username)
                    .collect();

                return Some(names);
            }
        }
    }

    None
}

/// Returns a `Vec::<String>` containing all the repositories for a given
/// organization in Artix Gitea
///
/// If there is an error reaching gitea, returns `None`
pub async fn get_organization_repositories(
    organization: &str,
    gitea_api_url: Option<&str>,
) -> Option<Vec<models::Repository>> {
    let client = Client::default();

    let response = client
        .get(format!(
            "{}/orgs/{}/repos",
            gitea_api_url.unwrap_or(config::GITEA_API_URL),
            organization
        ))
        .insert_header((
            "User-Agent",
            format!("{}/{}", config::USER_AGENT, config::VERSION),
        ))
        .send()
        .await;

    if let Ok(mut res) = response {
        if res.status().is_success() {
            return match res
                .json::<Vec<models::Repository>>()
                .limit(20 * 1024 * 1024)
                .await
            {
                Ok(data) => Some(data),
                Err(err) => {
                    println!("Error: {:?}", err);
                    None
                }
            };
        }
    }

    None
}

/*
   Mod tests
*/

#[cfg(test)]
mod test {
    use super::*;

    #[actix_web::test]
    async fn test_get_organization_id() {
        let orgs = get_organization_ids(None).await;
        assert!(orgs.is_some());
        if let Some(data) = orgs {
            assert!(!data.is_empty());
            for user_name in data {
                assert!(user_name.starts_with("packages") || user_name.starts_with("Universe"));
            }
        }
    }

    #[actix_web::test]
    async fn test_get_organization_repositories() {
        let repositories = get_organization_repositories("Universe", None)
            .await
            .unwrap();
        assert!(!repositories.is_empty());
        assert!(repositories.len() >= 17);
        for repo in &repositories {
            assert!(repo.id() > 0);
            assert!(!repo.name().is_empty());
            assert!(repo.full_name().contains("Universe"));
        }
    }
}
